<?php include 'inc/nav.php';  ?>

  <div class="container-fluid banner-top banner-aboutus">
    <div class="title-container container">
      <h1 style="color:white">WE ARE MEASURED BY WHAT WE CAN SOLVE</h1>
    </div>
  </div>
  
  <div class="container-fluid">
     <div class="container content-wrapper">
  
    <div class="row mb-20">
    
      <div class="col-sm-8 company-text" >
        <h2 class="dark-red"><strong>Our goal at Luxwood aims to bring more than just houses to people but instilling a sense of security, comfort and peacefulness that we believe every human being in the world deserves.</strong></h2>
        <br>
        <p>At Luxwood our mission is to leave a lasting impact and legacy in the construction industry so that those who have experienced our new living experience will promote our ideal of creating better homes.</p>
        <p>Luxwood global is an Australian-based engineering company specialising in the manufacture and distribution of Composite Polymer Extruded Panels.</p>
        <p>Our products are manufactured in a state-of-the-art facility in China using world-class extrusion techniques that provides excellent products without additional expenses factored into the price.  We are constantly striving to improve our products, and are undergoing extensive testing to further ensure the quality of our products continues. </p>
      </div>
      
      <div class="col-sm-4" id="carousel-bounding-box">
        <div id="myCarousel" class="carousel slide"> 
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
          </ol>
          
          <!--  item 1 -->
          <div class="carousel-inner">
          
            <div class="active item">
              <div class="testimonial-container">
                <div class="testimonial-wrapper">
                  <div class="testimonial">
                    <div class="corner-ribbon"><i class="fa fa-quote-left"></i></div>
                    <p>I had the privilege of working alongside this company and delivering housing solutions in Australia and overseas.  I can highly recommend this product if you are looking for a sound alternative building solution.</p>
                  </div>
                  <div class="logo-signiture">
                    <div class="logo-pic"> <img src="images/about/logo_IBSI.gif" width="80" height="40" alt="IBSI" /> </div>
                    <div class="logo-info"> <strong>Jacques De Bedout</strong><br>
                      <span style="font-weight: lighter"> Design Manager</span><br>
                      <a href="http://ibsi.com.au/">IBSI - Intelligent Building Systems International</a> </div>
                  </div>
                </div>
              </div>
            </div>
            
            <!--  item 2 -->
            <div class="item" >
              <div class="testimonial-container">
                <div class="testimonial-wrapper">
                  <div class="testimonial">
                    <div class="corner-ribbon"><i class="fa fa-quote-left"></i></div>
                    <p>I have had the pleasure of working with this company in Africa on a subsidized project for the Municipality. In trying circumstances and with mostly unskilled local labour, their level of this company’s professionalism has been exceptional, moreover their practical and hands on approach meant that everyone involved in the build of the  house was fully committed and engaged. First to arrive on site and last to leave, no issue too big and willing to overcome any problem there and then. Highly recommended.</p>
                  </div>
                  <div class="logo-signiture">
                    <div class="logo-pic"> <img src="images/about/logo_IBSI.gif" width="80" height="40" alt="IBSI" /> </div>
                    <div class="logo-info"> <strong>Francois Bruyns </strong><br>
                      <span style="font-weight: lighter"> Director </span><br>
                      <a href="http://ibsi.com.au/">IBSI -  the African subsidiary of ISBI</a> </div>
                  </div>
                </div>
              </div>
            </div>

          
        </div><!--/carousel-inner-->      
      </div><!--/carousel-->     
    </div><!--/col-->
</div><!--/row-->


<!-- certificate starts -->


<div class="row">
      <div class="col-sm-12">
        <div class="title-block" >
          <h5>LUXWOOD</h5>
          <h1> Certifications </h1>
        </div>
      </div>
    </div>
    
    <div class="row">
      <div class="col-md-6 certificate-tile"> <img src="images/about/certification_1.jpg" class="center"> </div>
      <div class="col-md-3 certificate-tile"> <img src="images/about/certification_2.jpg" class="center"> </div>
      <div class="col-md-3 certificate-tile"> <img src="images/about/certification_3.jpg" class="center"> </div>
    </div>
  </div>
</div>

<div class="container-fluid video-wrapper">
<div class="container content-wrapper">
    <div class="row">
      <div class="col-sm-6">
        <div class="video-block">
          <h2>FIREPROOF & FLAME RESISTANT</h2>
          <br>
          <p>Enjoy a product demonstration illustrating the fireproof and flame resistant feature of our patented composite polymer material.</p>
          <br>
          <p>Head to our new Videos page to view more product demonstrations and how to videos. You will also be able to watch 2 building demonstrations, a 3 bedroom home and an Ecocabin, both using Luxwood material.</p>
          <br>
          <a role="button" class="btn" href="videos.php">MORE VIDEOS</a> </div>
      </div>
      <div class="col-sm-6 video-block" id="promovid">
        <video width="90%" height="300" controls>
          <source src="images/about/BurnProcess.mp4" type="video/mp4">
        </video>
      </div>

</div></div></div>

<?php include 'inc/services.php';?>
<?php include 'inc/footer_contact.php';?>
<?php include 'inc/footer.php';?>