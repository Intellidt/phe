<?php include 'inc/nav.php';  ?>
  <div class="container-fluid banner-top banner-video">
    <div class="title-container container">
      <h1 style="color: white">VIDEOS</h1>
    </div>
  </div>
  
  
  <div class="container-fluid">
     <div class="container content-wrapper">
     
     <div class="row">
        <div class="col-sm-12">
          <div class="title-block" >
            <h5>LUXWOOD</h5>
            <h1> Luxwood Building Demonstration </h1>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-sm-6">
          <div class="video-frame">
            <video width="100%" height="350" controls class="mb-10">
              <source src="images/video/homecorp.mp4" type="video/mp4">
              <source src="images/video/homecorp.ogg" type="video/ogg">
              Your browser does not support the video tag. </video>
            <BR>
            <p class="mt-10">This video features the recently constructed Pacific 156 DG house for Homecorp, the dwelling features 3 bedrooms, 2 bathrooms, living dining and a double garage. The project was delivered in 8 weeks, using PHE Solutions + Luxwood building technologies.</p>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="video-frame">
            <video width="100%" height="350" controls class="mb-10">
              <source src="images/video/ecocabins.mp4" type="video/mp4">
              <source src="images/video/ecocabins.ogg" type="video/ogg">
              Your browser does not support the video tag. </video>
            <BR>
            <p>This Video features the construction sequence of the Osprey range of Ecocabins using PHE + Luxwood panel technology, it shows the start of the build process from Chassis and floors to walls being erected in position following by internal linings along with claddings. The sequence continues highlighting all aspects of the project delivery including transport logistics and site install.</p>
          </div>
        </div>
      </div>
      
      
      <div class="row">
        <div class="col-sm-12">
          <div class="title-block" >
            <h5>LUXWOOD</h5>
            <h1> Instructional Video </h1>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-sm-6">
          <div class="video-frame">
            <iframe width="100%" height="350" src="https://www.youtube.com/embed/ngHeJezdDl0?autoplay=0&showinfo=0&controls=1"> </iframe>
            <p>How to place the wall support U Chanel to House Base Frame</p>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="video-frame">
            <iframe width="100%" height="350" src="https://www.youtube.com/embed/OMkJx6TL5sE?autoplay=0&showinfo=0&controls=1"> </iframe>
            <p>How to lay the Floor Panels into the Base Frame</p>
          </div>
        </div>
       </div>
  
       
       <div class="row">
        <div class="col-sm-6">
          <div class="video-frame">
            <iframe width="100%" height="350" src="https://www.youtube.com/embed/WKv0JDGzAQU?autoplay=0&showinfo=0&controls=1"> </iframe>
            <p>How to level and prepare the house base frame before placing in the Luxwood floor Panels</p>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="video-frame">
            <iframe width="100%" height="350" src="https://www.youtube.com/embed/4ElD79vf2f8?autoplay=0&showinfo=0&controls=1"> </iframe>
            <p>Impressive demonstration of the insulation properties of the Polex™ and Luxwood® panels. You can hold the panel in your hand while temperatures on the other side of the panel can go has high as 1000º C</p>
          </div>
        </div>
    </div>
    
    
  </div>
  </div>
  <?php include 'inc/services.php';?>
  <?php include 'inc/footer.php';?>
