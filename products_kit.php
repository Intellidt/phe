<?php include 'inc/nav.php';  ?>

<div class="container-fluid banner-top banner-products-kithome">
  <div class="title-container container">
    <h1 style="color:white">PRODUCTS</h1>
  </div>
</div>

<div class="container-fluid">
  <div class="container content-wrapper">
  
    <div class="row">
      <div class="col-sm-12">
        <div class="title-block" >
          <h5>AWESOME PRODUCT</h5>
          <h1>PHE Luxwood Kit Homes</h1>
        </div>
      </div>
    </div>
    
      <!-- 3D Decking -->
  
    <div class="row product-carousel-row">
    
      <div class="col-sm-6">
        <P><strong>Grand House: </strong>Grand House is a luxurious design with two spacious bedrooms. The front porch and back deck area are prefect for those who enjoy entertaining or the nature outdoors. The open plan design of the dining and living area provides a very practical and beneficial space. Our exceptional thermal barrier mean cold winter nights or warm summer morning won’t be a problem.</P>
        <p><strong>Heather House : </strong>The Heather house is a modern masterpiece. Boasting four spacious bedrooms, two bathrooms, and a laundry room. The front and back porch are perfect for entertaining. The lounge and dining area are designed to capture every moment of the beautiful awaked sunshine.</p>
        <p><strong>Islander Cabin: </strong>Islander Cabin’s simple and modern design emphasizes simplicity and green living. The thermal efficiency of our panel and cladding as well as our JELI Roofing create a home that needs no heating or cooling systems. Our Luxwood Helical Screw Piles allows the environment to live harmoniously with you.</p>
        <p><strong>Theodor School: </strong>Theodor Herzl School has three classrooms, each classroom containing its own storeroom. The verandas are welcomed by students on all year round. Fending off heat during the warm summer days as well as the rain in the rainy winter seasons. Air conditions are not necessary, due to our fantastic thermal barrier.</p>

      </div>
      
      <div class="col-sm-6">
          <div id="productCarousel" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators carousel-indicators-product">
                  <li data-target="#productCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#productCarousel" data-slide-to="1"></li>
                  <li data-target="#productCarousel" data-slide-to="2"></li>
                  <li data-target="#productCarousel" data-slide-to="3"></li>
                  <li data-target="#productCarousel" data-slide-to="4"></li>

                                </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                  <div class="item active"><img src="images/products/kithome/phe_kithome_1.jpg" alt=""></div>
                  <div class="item"><img src="images/products/kithome/phe_kithome_2.jpg" alt=""></div>
                  <div class="item"><img src="images/products/kithome/phe_kithome_3.jpg" alt=""></div>
                  <div class="item"><img src="images/products/kithome/phe_kithome_4.jpg" alt=""></div>
                  <div class="item"><img src="images/products/kithome/phe_kithome_5.jpg" alt=""></div>
              </div>
          </div>
      </div><!--/col-->
      
</div><!--/row-->

</div>
</div>

<?php include 'inc/highlights.php';?>
<?php include 'inc/services.php';?>
<?php include 'inc/footer.php';?>