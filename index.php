<?php include 'inc/nav.php';  ?>

<!-- Carousel starts -->


  <div class="container-fluid product">
    <div class="home_banner">
      <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
      
        <ol class="carousel-indicators">
          <li data-target="#carousel" data-slide-to="0" class="active"></li>
          <li data-target="#carousel" data-slide-to="1"></li>
          <li data-target="#carousel" data-slide-to="2"></li>
        </ol>

            <div class="carousel-inner">
              <div class="active item back">
                  <div class="carousel-text">
                      <h1>Innovative Building Technology &amp; Affordable Housing</h1>
                      <h4>We are the designers and manufacturers of Luxwood’s innovative building system and Kit Homes.<br> We help families to bring back a sense of safety, comfort and peacefulness into their homes, one structure at a time.</h4><br>
                      
                  <a role="button" class="btn" href="building_kit_home.php">MORE INFO</a> </div>
              </div>
              <div class="item back">
                  <div class="carousel-text"><h1>International Leader in Composite Polymer Extruding Panels</h1>
                  <h4>The high-performance engineered building components incorporates interlocking joints that create seamless walls and floors.</h4><BR>
  
                  <a role="button" class="btn" href="building_phe_panels.php">MORE INFO</a></div>
              </div>
              <div class="item back">
                <div class="carousel-text"><h1>Recyclable, Sustainable, Low Maintenance </h1>
                    <h4>Our Innovative technology has allowed us to create an atmosphere that merges both beauty and functionality </h4><br>
                   
                  <a role="button" class="btn" href="products_decking.php">MORE INFO</a> </div>
              </div>
            </div>
        
      </div>
    </div>
    
    </div>
  
<!-- Carousel ends -->


<?php include 'inc/features.php';?>
 
 
  <!-- composit material starts -->
  
<div class="container-fluid banner-home-mid">
<div class="container content-wrapper">

      <div class="row">
        <div class="col-sm-5">
          <div class="title-block">
            <h5>OUR PATENTED</h5>
            <h1 style="color:white;">Composite Material</h1>
          </div>
          <p>We make eco-friendly composite building materials from an innovative blend of 95% recycled wood and plastic – that's almost the whole thing. And we’ve been this way for more than 20 years, because in the end, all of us want to know that we’ve done our part.</p>
        </div>
        <div class="col-sm-7">
          <ul class="highlights-list">
            <li>
              <div class="img-header"> <img src="images/home/icon_composite_formaldehyde.png" width="60" height="60" alt="Non Formaldehyde" />
                <p>Non Formaldehyde</p>
              </div>
            </li>
            <li>
              <div class="img-header"> <img src="images/home/icon_composite_fireproof.png" width="60" height="60" alt="Fireproof" />
                <p>Fireproof</p>
              </div>
            </li>
            <li>
              <div class="img-header"> <img src="images/home/icon_composite_waterproof.png" width="60" height="60" alt="Waterproof" />
                <p>Waterproof</p>
              </div>
            </li>
            <li>
              <div class="img-header"> <img src="images/home/icon_composite_insectproof.png" width="60" height="60" alt="Insectproof" />
                <p>Pest Resistant</p>
              </div>
            </li>
            <li>
              <div class="img-header"> <img src="images/home/icon_composite_corrosion.png" width="60" height="60" alt="Corrosion Protection" />
                <p>Corrosion Protection</p>
              </div>
            </li>
            <li>
              <div class="img-header"> <img src="images/home/icon_composite_striking.png" width="60" height="60" alt="Striking Resistant" />
                <p>Striking Resistant</p>
              </div>
            </li>
            <li>
              <div class="img-header"><img src="images/home/icon_composite_recycle.png" width="60" height="60" alt="Recycle" />
                <p>Recycle</p>
              </div>
            </li>
            <li>
              <div class="img-header"> <img src="images/home/icon_composite_sound.png" width="60" height="60" alt="Sound Insulation" />
                <p>Sound Insulation</p>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  
  <!-- composit material ends -->
  
  
  <!-- building app starts -->
  
<div class="container-fluid">
<div class="container content-wrapper">

      <div class="row">
        <div class="col-sm-5">
          <div class="title-block">
            <h5>SUITABLE FOR MANY</h5>
            <h1>Building Applications</h1>
          </div>
        </div>
        
        <div class="col-sm-7">
          <p style="margin-left: 8%;">Our products can be used in many scenarios including:</p>
          <ul class="scenario-list">
            <li>Emergency Housing</li>
            <li>Eco Housing</li>
            <li>Multi-purpose Mining Camps </li>
            <li>Schools and Halls</li>
            <li>and many more</li>
          </ul>
        </div>

    </div>
  </div></div>
  
  <!-- building app ends -->  
 
   <!-- build house starts --> 
  
 <div class="container-fluid" style="background-color: #e9e9e9;">
 
  <div class="container content-wrapper">

      <div class="row">
        <div class="col-sm-6">
          <div class="title-block">
            <h5>HOW TO</h5>
            <h1>Build a house in 1 day</h1>
          </div>
          A single unit with two bedrooms can be completed by a team of 8 Luxwood trained installers.<BR>
          <strong>AFTER <span class="dark-red">24 HOURS</span> THE HOUSE IS READY FOR OPTIONAL EXTERNAL & INTERNAL FINISHING.</strong>

          <!--
          <table width="80%" border="0" cellpadding="0" cellspacing="0" align="center">
              <tr>
                <td width="40%"><strong>Team: </strong></td>
                <td width="60%">8 People</td>
              </tr>
              <tr>
                <td><strong>House:</strong></td>
                <td>2 Bedroom</td>
              </tr>
              <tr>
                <td><strong>Build Time:</strong></td>
                <td>1 Day</td>
              </tr>
              <tr>
                <td><strong>Start Construction:</strong></td>
                <td>7:30 am</td>
              </tr>
              <tr>
                <td><strong>Lunch Break:</strong></td>
                <td>1 Hour</td>
              </tr>
              <tr>
                <td><strong>House Completion:</strong></td>
                <td>4:30 pm</td>
              </tr>
            </table>-->
        </div>
        
        <div class="col-sm-6">
          <video class="video-player" width="100%" height="300" frameborder="0" controls>
            <source src="images/video/1day.mp4" type="video/mp4">
            <source src="images/video/1day.ogg" type="video/ogg">
            Your browser does not support the video tag. </video>
        </div>

    </div>
  </div>
  </div>
  
   <!-- building house ends --> 
  
  <!-- bottom section starts --> 
  
  <div class="container-fluid banner-home-btm" style="position: relative;">
    <div class="container overlay-cover">
    
        <div class="row mb-50">
        
          <div class="col-sm-12">
            <div class="overlay">
              <p>Here are just some of the building application our Composite Polymer is suitable for.</p>
              <ul>
                <li><a href="products_kit.php">KIT HOMES</a></li>
                <li><a href="products_decking.php">DECKING</a></li>
                <li><a href="products_cladding.php">CLADDING</a></li>
              </ul>
            </div>
          </div>
          
        </div>
      </div>
    </div>
    
  <!-- bottom section ends --> 

<?php include 'inc/services.php';?>
  <?php include 'inc/footer_contact.php';?>
  <?php include 'inc/footer.php';?>
