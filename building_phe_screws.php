<?php include 'inc/nav.php';  ?>

<div class="container-fluid banner-top banner-build-helical">
  <div class="title-container container">
    <h1 style="color:white">BUILDING SYSTEM</h1>
  </div>
</div>

<div class="container-fluid content-wrapper">
    <div class="container">

	<div class="row">
      <div class="col-sm-12">
        <div class="title-block" >
          <h5>BUILDING APPLICATION</h5>
          <h1>PHE Luxwood Screw Piles</h1>
        </div>
      </div>
    </div>
    
	<div class="row">
	<div class="col-sm-4 building-system-img">
    <img src="images/building_system/helical_screws.jpg" alt="PHE Building System Helical Screws" class="img-responsive" width="300" height="300" >
	</div>
	<div class="col-sm-8 building-system-content">
      <p>The PHE Luxwood Screw Piles are used as an anchoring system to build deep foundations and are used in a variety of new ways. Other than just anchoring down structures, large or small, they can be used to help secure fences deep into the ground, securing mounting brackets or help with installing boardwalks well above the normal water line.</p>
        <ul class="specs-list">
						<li>PHE Luxwood Screw Piles is generally much quicker and far more cost effective than other deep foundation solution.</li>
						<li>PHE Luxwood Screw Piles are much easier to install than a conventional foundation; an ideal solution in wetland areas and confined spaces.</li>
                        <li>Unlike most other foundation methods, PHE Luxwood Screw Piles can also be removed and recycled with very little effort.</li>
        </ul>
	</div>
	</div>
	
  </div>
</div>
  

<?php include 'inc/services.php';?>
<?php include 'inc/footer.php';?>

