<!DOCTYPE html>
<html lang="en">
<head>
<title>PHE SOLUTIONS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name = "format-detection" content = "telephone=no" />
<meta name="keywords" content="phe, solutions, pvt, ltd">
<meta name="description" content="phe, solutions, pvt, ltd">
<meta property="og:image" content="images/logo_phe_solutions_s.png" />
<meta property="og:title" content="PHE SOLUTIONS" />
<meta name="description" property="phe, solutions, pvt, ltd">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">

<link href="http://vjs.zencdn.net/4.12/video-js.css" rel="stylesheet">
<script src="http://vjs.zencdn.net/4.12/video.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:bold|Roboto:bold,regular,light" rel="stylesheet"> 
<!-- Latest compiled and minified JavaScript -->
<script src="https://code.jquery.com/jquery-3.1.0.js"   integrity="sha256-slogkvB1K3VOkzAI8QITxV3VzpOnkeNVsKvtkYLMjfk="   crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" >
</head>
<body>

<!--<div class="page_wrapper">-->
<nav class="navbar navbar-default">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.php"><img src="images/logo_phe_solutions.png"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right" >
        <li>
        <a data-toggle="collapse" href="#about">About Us</a></li>
        <li>
          <a data-toggle="collapse" href="#build">Building System</a>
          
        </li>
		<li>
          <a data-toggle="collapse" href="#products">Products</a>
          
        </li>
        <li><a href="projects.php">Projects</a></li>
        <li><a href="videos.php">Videos</a></li>
		<li><a href="contact.php">Contact Us</a></li>
      </ul>
    </div>
  </div>
</nav>

<div id="about" class="collapse external-nav">
		  <ul>
            <li><a href="about_company.php">Our Company</a></li>
            <li><a href="about_member.php">Luxwood Family</a></li>
          </ul>
</div>

<div id="build" class="collapse external-nav">
		  <ul>
            <li><a href="building_kit_home.php">PHE Luxwood Kit Homes</a></li>
            <li><a href="building_phe_panels.php">PHE Luxwood Panels</a></li>
            <li><a href="building_phe_screws.php">PHE Luxwood Screw Piles</a></li>
			<li><a href="building_jeli_roofing.php">Jeli Roofing</a></li>
            <li><a href="building_luxwood_digger.php">Luxwood Digger</a></li>
            <li><a href="building_assembly.php">Assembly</a></li>
			<li><a href="building_phe_training.php">PHE Training Course</a></li>
          </ul>
</div>
<div id="products" class="collapse external-nav">
		  <ul>
            <li><a href="products_kit.php">PHE Luxwood Kit Homes</a></li>
            <li><a href="products_decking.php">PHE Luxwood Decking</a></li>
            <li><a href="products_cladding.php">PHE Luxwood Cladding</a></li>
          </ul>
</div>

<script>
    $('li').click( function(e) {
        //jQuery('.collapse').collapse('hide');
        if ($('.collapse').hasClass("in")) {$('.collapse').removeClass('in');}
    });
</script>