
<div class="container-fluid content-wrapper"  style="background-color: #f0f0f0">
<div class="container">
<div class="row">
<div class="col-sm-12 service-header">
<h2 style="color: #888">We pride ourselves in producing quality products and services in a safe and compliant workspace.</h2>
</div>
</div>
<div class="row">
<div class="col-sm-3 adv-tile">
<div class="img-header img-description">
<img src="images/icon_consultation.png" width="60" height="60" alt="consultation" />
<h3>FREE CONSULTATION</h3>
</div>
<p>Contact us to discuss your Kit Home needs further.</p>
</div>
<div class="col-sm-3 adv-tile">
<div class="img-header img-description">
<img src="images/icon_timeframe.png" width="60" height="60" alt="timeframe" />
<h3>TIMEFRAMES</h3>
</div>
<p>Our system helps you work to your deadlines.</p>
</div>
<div class="col-sm-3 adv-tile">
<div class="img-header img-description">
<img src="images/icon_price.png" width="60" height="60" alt="price" />
<h3>PRICE COMPETITIVE</h3>
</div>
<p>Our Kit Homes are cost effective to many budgets.</p>
</div>
<div class="col-sm-3 adv-tile">
<div class="img-header img-description">
<img src="images/icon_solution.png" width="60" height="60" alt="solution" />
<h3>INNOVATIVE SOLUTIONS</h3>
</div>
<p>Providing innovative and effective solutions for your home.</p>
</div>
</div>
</div>
</div>
