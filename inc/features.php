<!-- features starts -->

<div class="container-fluid">
<div class="container content-wrapper">
    	<div class="row">
        <div class="col-sm-12 mb-0">
        <p>
        <div class="title-block">
            <h5>OUR</h5>
            <h1>Innovative Features</h1>
          </div></p>
          
        </div>
      </div>
      
 <!-- feature items -->
      
      
      <div class="row">
      
            <div class="col-sm-2 adv-tile">
              <div class="img-header"><img src="images/home/icon_home_1.png" width="60" height="60" alt="Swift Construction" /></div>
              <p>Swift Construction</p>
            </div>
            
            <div class="col-sm-2 adv-tile">
              <div class="img-header"><img src="images/home/icon_home_2.png" width="60" height="60" alt="Fire Retardant" /></div>
              <p>Waterproof</p>
            </div>
            
            <div class="col-sm-2 adv-tile">
              <div class="img-header"><img src="images/home/icon_home_3.png" width="60" height="60" alt="Great Acoustic Barrier" /></div>
              <p>Fire &amp; Smoke Retardant</p>
            </div>

            <div class="col-sm-2 adv-tile">
              <div class="img-header"><img src="images/home/icon_home_4.png" width="60" height="60" alt="Great Acoustic Barrier" /></div>
              <p>Energy Saving &amp; Heat Prevention</p>
            </div>
            
            <div class="col-sm-2 adv-tile">
              <div class="img-header"><img src="images/home/icon_home_5.png" width="60" height="60" alt="Great Acoustic Barrier" /></div>
              <p>Transportable</p>
            </div>
            
            <div class="col-sm-2 adv-tile">
              <div class="img-header"><img src="images/home/icon_home_6.png" width="60" height="60" alt="Great Acoustic Barrier" /></div>
              <p>Earthquake &amp; Hurricane tested</p>
            </div>
            
            <div class="col-sm-2 adv-tile">
              <div class="img-header"><img src="images/home/icon_home_7.png" width="60" height="60" alt="Great Acoustic Barrier" /></div>
              <p>Environmentally Friendly</p>
            </div>
            
            <div class="col-sm-2 adv-tile">
              <div class="img-header"><img src="images/home/icon_home_8.png" width="60" height="60" alt="Great Acoustic Barrier" /></div>
              <p>>Construction Savings</p>
            </div>
            
            <div class="col-sm-2 adv-tile">
              <div class="img-header"><img src="images/home/icon_home_9.png" width="60" height="60" alt="Great Acoustic Barrier" /></div>
              <p>High Rate Acoustic</p>
            </div>
            
            <div class="col-sm-2 adv-tile">
              <div class="img-header"><img src="images/home/icon_home_10.png" width="60" height="60" alt="Great Acoustic Barrier" /></div>
              <p>Pest Resistant</p>
            </div>
            
            <div class="col-sm-2 adv-tile">
              <div class="img-header"><img src="images/home/icon_home_11.png" width="60" height="60" alt="Great Acoustic Barrier" /></div>
              <p>Low Maintenance Cost</p>
            </div>
            
            <div class="col-sm-2 adv-tile">
              <div class="img-header"><img src="images/home/icon_home_12.png" width="60" height="60" alt="Great Acoustic Barrier" /></div>
              <p>Durability &amp; Light Weight</p>
            </div>
  
  </div>
  </div>
  </div>
  
  
 <!-- features ends -->