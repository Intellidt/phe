<div class="banner-home-mid container-fluid">
<div class="container content-wrapper">

<div class="row">

<div class="col-sm-12">

<ul class="highlights-list-about">
          <li><div class="img-header">
			<img src="images/home/icon_composite_formaldehyde.png" width="60" height="60" alt="Non Formaldehyde" />
            <p>Non Formaldehyde</p></div></li>
          <li><div class="img-header">
		  <img src="images/home/icon_composite_fireproof.png" width="60" height="60" alt="Fireproof" />
            <p>Fireproof</p></div></li>
          <li><div class="img-header">
		  <img src="images/home/icon_composite_waterproof.png" width="60" height="60" alt="Waterproof" />
            <p>Waterproof</p></div></li>
          <li><div class="img-header">
		  <img src="images/home/icon_composite_insectproof.png" width="60" height="60" alt="Insectproof" />
            <p>Insectproof</p></div></li>
          <li><div class="img-header">
		  <img src="images/home/icon_composite_corrosion.png" width="60" height="60" alt="Corrosion Protection" />
            <p>Corrosion Protection</p></div></li>
          <li><div class="img-header">
		  <img src="images/home/icon_composite_striking.png" width="60" height="60" alt="Striking Resistant" />
            <p>Striking Resistant</p></div></li>
          <li><div class="img-header">
		  <img src="images/home/icon_composite_recycle.png" width="60" height="60" alt="Recycle" />
            <p>Recycle</p></div></li>
          <li><div class="img-header">
		  <img src="images/home/icon_composite_sound.png" width="60" height="60" alt="Sound Insulation" />
            <p>Sound Insulation</p></div></li>
        </ul>
</div>

</div>

</div>
</div>