<?php include 'inc/nav.php';  ?>

<div class="container-fluid banner-top banner-build-training">
    <div class="title-container container">
        <h1 style="color:white">PHE TRAINING COURSE</h1>
    </div>
</div>


<div class="container-fluid content-wrapper">
    <div class="container">

        <div class="row mb-50">
            <div class="col-sm-12">
                <div class="title-block" >
                    <h5>M.A.W</h5>
                    <h1>PHE Luxwood Building System Course</h1>
                </div>
                <p>We truly believe that knowledge is power. That’s why we offer the course to empower others with a new skill and a desire to learn further.</p>
            </div>
        </div>
        
        
        <div class="row">
            <div class="col-sm-4 building-system-img">
                <img src="images/building_system/phe_training.jpg" alt="PHE Building System Training" class="img-responsive" width="300" height="300">
            </div>
            <div class="col-sm-8 building-system-content" id="panel">
            
                        <div class="panel-group" id="accordion">

                            <!--<a data-toggle="collapse" data-parent="#accordion" href="#collapse1"> -->

                            <div class="panel panel">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <button id="button2" type="button" class="btn" data-toggle="collapse" data-target="#collapse2" data-parent="#accordion">
                                            <span class="glyphicon glyphicon-plus"></span></button>
                                        <h5 class="product-properties">What will I learn from this course? </h5>
                                    </div>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>The M.A.W PHE Luxwood Building System Course will provide you with the knowledge and practical experience on how to build with our systems.</p>
                                    </div>
                                </div>
                            </div>


                            <div class="panel panel">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <button id="button3" type="button" class="btn" data-toggle="collapse" data-target="#collapse3" data-parent="#accordion">
                                            <span class="glyphicon glyphicon-plus"></span></button>
                                        <h5 class="product-properties">When is this Course?</h5>
                                    </div>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>The course is currently three weeks long. (Three Saturdays). Custom courses can also be done for 10 participants in a week.</p>
                                    </div>
                                </div>
                            </div>


                            <div class="panel panel">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <button id="button4" type="button" class="btn" data-toggle="collapse" data-target="#collapse4" data-parent="#accordion">
                                            <span class="glyphicon glyphicon-plus"></span></button>
                                        <h5 class="product-properties">Where is this Course?</h5>

                                    </div>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Our course is held in Cape Town, South Africa. We will be opening up courses in New Zealand and the USA shortly.</p>
                                    </div>
                                </div>
                            </div>


                            <div class="panel panel">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <button id="button5" type="button" class="btn" data-toggle="collapse" data-target="#collapse5" data-parent="#accordion">
                                            <span class="glyphicon glyphicon-plus"></span></button>
                                        <h5 class="product-properties">Who can participate in this Course?</h5>

                                    </div>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Men or woman over the age of 18, no previous qualifications are needed.</p>
                                    </div>
                                </div>
                            </div>


                            <div class="panel panel">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <button id="button6" type="button" class="btn" data-toggle="collapse" data-target="#collapse6" data-parent="#accordion">
                                            <span class="glyphicon glyphicon-plus"></span></button>
                                        <h5 class="product-properties">What is the charge of this Course ?</h5>
                                    </div>
                                </div>
                                <div id="collapse6" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>Our course has a fee of R100 South African rand.</p>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
</div>

<?php include 'inc/services.php';?>
<?php include 'inc/footer.php';?>


<script>
    $(function(){
        $('.panel-collapse').on('hide.bs.collapse', function () {
            var currentId = $(this).attr('id');
            var numb = currentId.match(/\d+$/)[0];
            var buttonId="#button".concat(numb);
            $(buttonId).html('<span class="glyphicon glyphicon-plus"></span>');
        })
        $('.panel-collapse').on('show.bs.collapse', function () {
            var currentId = $(this).attr('id');
            var numb = currentId.match(/\d+$/)[0];
            var buttonId="#button".concat(numb);
            $(buttonId).html('<span class="glyphicon glyphicon-minus"></span>');
        })
    })
</script>