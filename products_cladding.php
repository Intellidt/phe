<?php include 'inc/nav.php';  ?>

<div class="container-fluid banner-top banner-products-cladding">
  <div class="title-container container">
    <h1 style="color:white">PRODUCTS</h1>
  </div>
</div>

<div class="container-fluid">
  <div class="container content-wrapper">
  
    <div class="row">
      <div class="col-sm-12">
        <div class="title-block" >
          <h5>AWESOME PRODUCT</h5>
          <h1>PHE Luxwood Cladding</h1>
        </div>
      </div>
    </div>

  
    <div class="row">
    
      <div class="col-sm-6">
        <P>PHE Luxwood Polex Board Cladding is a new kind of low-carbon cladding material with 3 functions: cladding, insulation and decoration. It is made from cement and plant fiber without asbestos. It is produced by vacuum and hollow extrusion molding process which is unique, well finished by double stages autoclaved curing, coating and packing process.</P>
        <p><strong>Application:  </strong><BR>
        Houses , Business, Building , Catering , Academy , Pavilion etc.
Product Features: 
</p>
        <Ul>
        <li>• Stable quality </li>
        <li>• Sound and thermal insulation
        <li>• Fire Proof</li>
        <li>• Flexible connections</li>
        <li>• Fast construction</li>
        <li>• Light and strong</li>
        </Ul>
        
        <p><strong>Specification :</strong></p>
        <p class="mb-0">Hollow: </p>
        <ul class="specs-list">
        <li>3000*300*15mm</li>
        <li>3000*450*18mm</li>
        <li>3000*600*26mm</li>
        </ul>
        <p class="mb-0">Solid:</p>
        <ul class="specs-list">
        <li>3000*460*16mm</li>
        </ul>
      </div>
      
      <div class="col-sm-6">
         <img src="images/products/cladding/products_cladding.jpg" alt="" class="img-responsive" />
          </div>
      </div><!--/col-->
      
</div><!--/row-->

</div>
</div>

<!-- certification starts -->

<div class="container-fluid color-display-cladding">
  <div class="container content-wrapper">
    <div class="row">
      <div class="col-sm-3">
        <div class="title-block">
          <h5>CLADDING </h5>
          <h1 style="color:white;">Color Display</h1>
        </div>
      </div>
      <div class="col-sm-9">
        <ul class="highlights-list color-img">
          <li> <img src="images/products/cladding/cladding_color_display_1.png" alt="" class="img-responsive" />
          </li>
          <li> <img src="images/products/cladding/cladding_color_display_2.png" alt="" class="img-responsive" />
          </li>
          <li> <img src="images/products/cladding/cladding_color_display_3.png" alt="" class="img-responsive" />
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="container content-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <div class="title-block" >
          <h5>CLADDING</h5>
          <h1> Accessories Display </h1>
        </div>
      </div>
    </div>
    <div class="row">
      <ul class="adv-tile">
        <li class="img-header accessories"> <img src="images/products/cladding/cladding_accessories_1.jpg" alt="Floor" />
          <h4>Floor</h4>
        </li>
        <li class="img-header accessories"> <img src="images/products/cladding/cladding_accessories_2.jpg" alt="Skeleton" />
          <h4>Skeleton</h4>
        </li>
        <li class="img-header accessories"> <img src="images/products/cladding/cladding_accessories_3.jpg" alt="Fastener" />
          <h4>Fastener</h4>
        </li>
        <li class="img-header accessories"> <img src="images/products/cladding/cladding_accessories_4.jpg" alt="Covered Edge" />
          <h4>Covered Edge</h4>
        </li>
        <li class="img-header accessories"> <img src="images/products/cladding/cladding_accessories_5.jpg" alt="Covered Edge" />
          <h4>Covered xyz</h4>
        </li>
      </ul>
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="container content-wraper">
    <div class="row">
      <div class="col-sm-12">
        <div class="title-block" >
          <h5>QUALITY</h5>
          <h1> Certifications </h1>
        </div>
      </div>
    </div>
    <div class="certificate-container mb-50">
      <div class="certificate-1"><img src="images/products/cladding/cladding_cert_1.jpg" /> </div>
      <div class="certificate-2"><img src="images/products/cladding/cladding_cert_2.jpg" /> </div>
    </div>
  </div>
</div>


<?php include 'inc/highlights.php';?>
<?php include 'inc/services.php';?>
<?php include 'inc/footer.php';?>