<?php include 'inc/nav.php';  ?>

  <div class="container-fluid banner-top banner-build-roofing">
    <div class="title-container container">
      <h1 style="color:white">BUILDING SYSTEM</h1>
    </div>
  </div>
  
  
  <div class="container-fluid content-wrapper">
      <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="title-block" >
          <h5>BUILDING APPLICATION</h5>
          <h1>Luxwood Jeli Roofing </h1>
        </div>
      </div>
    </div>
    
    <div class="row">
      <div class="col-sm-4 building-system-img"> <img src="images/building_system/jeli_roofing.jpg" alt="PHE Building System Jeli Roofing" class="img-responsive" width="300" height="300"> </div>
      <div class="col-sm-8 building-system-content">
        <p>Jeli Glass Fiber reinforced synthetic resin tile is design by four layers of composite coextrusion technology. Between each plates contains a high strength glass fiber cloth squeezed between two layers of resin plates. Greatly improving the tensile, flexural and impact resistance of the resin tile.</p>
          <h3 class="product-use" style="text-align: left">PRODUCT PROPERTIES</h3>
          <ul class="product-property-list">
          <li>More stable under severe weather variations</li>
          <li>Available for all profiles</li>
          <li>Customizable size and colour</li>
          <li>Abundant accessories</li>
          <li>Easy installation, Save labour Cost</li>
          <li>Supply long purlin distance</li>
          <li>Chemical resistance</li>
          <li>UV and water proof</li>
          <li>Applicable residential, commercial and industrial roof</li>
        </ul>
        <ul class="product-property-list right-list">
            <li>Heat Resistance</li>
          <li>Fire Proof</li>
          <li>Environment Friendly</li>
          <li>Weather Resistance</li>
          <li>Sound Proof</li>
          <li>Longer Life Span</li>
          <li>Impact Resistance</li>
          <li>Safe Loading</li>
        </ul>
      </div>
    </div>
</div>
  </div>


<?php include 'inc/services.php';?>
<?php include 'inc/footer.php';?>
