<?php include 'inc/nav.php';  ?>

  <div class="container-fluid banner-top banner-build-cpep">
    <div class="title-container container">
      <h1 style="color:white">BUILDING SYSTEM</h1>
    </div>
  </div>
  
  <div class="container-fluid content-wrapper">
      <div class="container">
  
    <div class="row">
      <div class="col-sm-12">
        <div class="title-block" >
          <h5>BUILDING SYSTEM</h5>
          <h1>PHE Luxwood Panels</h1>
        </div>
      </div>
    </div>
    
    <div class="row">
      <div class="col-sm-4 building-system-img"><img src="images/building_system/cpep_panel.jpg" alt="PHE Building System CPEP Panels" class="img-responsive" width="300" height="300"></div>
      
      <div class="col-sm-8">
        <ul class="nav nav-tabs nav-tabs-divider">
          <li role="presentation" class="active"><a href="#tab1" data-toggle="tab">
            <h4>Wall Panels</h4>
            </a></li>
            <li role="presentation"><a href="#tab2" data-toggle="tab">
                    <h4>Product Properties</h4>
                </a></li>
          <li role="presentation"><a href="#tab3" data-toggle="tab">
            <h4>FAQ</h4>
            </a></li>
          <li role="presentation"><a href="#tab4" data-toggle="tab">
            <h4>Doors &amp; Windows</h4>
            </a></li>
        </ul>
        <div class="tab-content"> 
          
          <!-- Wall Panels -->
          
          <div class="tab-pane fade in active" id="tab1">
            <h3 class="product-use">Component and Technology</h3>
            <p mt-30>PHE Luxwood Panels are engineered in Australia and manufactured in our state-of-the-art plant in China using world-class extrusion techniques. Together they offer a solution for affordable housing using improved technology that undergoes extensive testing to ensure the quality and the structure are not sacrificed.</p>
            <p mt-30>The high-performance engineered building components incorporates interlocking joints that create seamless walls and floors. The panels integral structural capacity, eliminates the need for load bearing frames and walls and can be built directly onto a concrete slab or on an elevated steel-framed floor platform.</p>
            <p mt-30>PHE Luxwood Panels uses phonlic/polyvinyl chloride as adhesive, along with flying ash tailing or other inorganic material as main component. The panels are also mixed with wood fiber, glass fiber, fire retardant and other additives and formed by extrusion technology.</p>
            <p mt-50>Through the use of PHE Luxwood Panels construction cost and labour cost are significantly reduced.</p>
            <h3 class="product-use">Application</h3>
            <p mt-50>Popularly used as a wall structure and roof structure in civil buildings, business buildings, public service, temporary housing, gyms, hotels, hospitals, departments and industry buildings.</p>
            <br>


          </div>
          
          <!-- end of Wall Panels --> 
          <!----product Properties ----->
            <div class="tab-pane fade in" id="tab2">

                <div class="row">
                    <div class="col-sm-12" id="panel">
                        <div class="panel-group" id="accordion">

                            <!--<a data-toggle="collapse" data-parent="#accordion" href="#collapse1"> -->
<!-- extra --><!--
                            <div class="panel panel">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <button id="button1" type="button" class="btn" data-toggle="collapse" data-target="#collapse1" data-parent="#accordion"> <span class="glyphicon glyphicon-plus"></span></button>
                                        <h5 class="product-properties">Quick &amp; Easy Installation </h5>
                                    </div>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="product-property-list">
                                            <li>The Panels are ergonomically designed to efficiently speed up the building process.</li>
                                            <li>Utilising Luxwood’s Building System, installation of the panels becomes a breeze.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <button id="button2" type="button" class="btn" data-toggle="collapse" data-target="#collapse2" data-parent="#accordion"> <span class="glyphicon glyphicon-plus"></span></button>
                                        <h5 class="product-properties">Waterproof</h5>
                                    </div>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        Panels are all waterproof and does not create condensation.
                                    </div>
                                </div>
                            </div>-->
                            <div class="panel panel">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <button id="button3" type="button" class="btn" data-toggle="collapse" data-target="#collapse3" data-parent="#accordion"> <span class="glyphicon glyphicon-plus"></span></button>
                                        <h5 class="product-properties">Fire &amp; Smoke Retardant </h5>
                                    </div>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="product-property-list">
                                            <li>The flame retardant and smoke preventative additives in the recipe can effectively stop the wall panel being ignited.</li>
                                            <li>Test reports show fire resistance of the wall panel will exceed two hours and non-combustable Grade-A product under EN standards.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <button id="button4" type="button" class="btn" data-toggle="collapse" data-target="#collapse4" data-parent="#accordion"> <span class="glyphicon glyphicon-plus"></span></button>
                                        <h5 class="product-properties">Environmentally Friendly</h5>
                                    </div>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="product-property-list">
                                            <li>The limits of radio-nuclides formaldehyde content in panel forms no hard to health of humans or animals.</li>
                                            <li>The panel is recyclable and can be granulated back into product many years after used and therefore no construction waste has been created.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <button id="button5" type="button" class="btn" data-toggle="collapse" data-target="#collapse5" data-parent="#accordion"> <span class="glyphicon glyphicon-plus"></span></button>
                                        <h5 class="product-properties">Construction Savings </h5>
                                    </div>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="product-property-list">
                                            <li>Wall construction reduces or even eliminates the need of steel structure.</li>
                                            <li>Reduces the need for heavy lifting equipment for construction.</li>
                                            <li>Wall panels can be installed faster, compared to traditional building processes.</li>
                                            <li>Wall construction is 70% – 80% quicker, results in saving of 80% in labor costs.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            
<!-- extra --><!--                            
                            <div class="panel panel">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <button id="button6" type="button" class="btn" data-toggle="collapse" data-target="#collapse6" data-parent="#accordion"> <span class="glyphicon glyphicon-plus"></span></button>
                                        <h5 class="product-properties">High Rate Acoustic</h5>
                                    </div>
                                </div>
                                <div id="collapse6" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        Panels provide similar acoustic performance to lightweight stud walls commonly used in residential applications.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <button id="button7" type="button" class="btn" data-toggle="collapse" data-target="#collapse7" data-parent="#accordion"> <span class="glyphicon glyphicon-plus"></span></button>
                                        <h5 class="product-properties">Pest Resistant </h5>
                                    </div>
                                </div>
                                <div id="collapse7" class="panel-collapse collapse">
                                    <div class="panel-body">
                                            The Panel has no nutritional value as a food source so will not attract vermin. It is recommended to maintain a suitable pest control regime as a preventative measure.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <button id="button8" type="button" class="btn" data-toggle="collapse" data-target="#collapse8" data-parent="#accordion"> <span class="glyphicon glyphicon-plus"></span></button>
                                        <h5 class="product-properties">Low Maintenance Cost</h5>
                                    </div>
                                </div>
                                <div id="collapse8" class="panel-collapse collapse">
                                    <div class="panel-body">
                                            Our products are flexible and durable – you never need to get out the paintbrush providing low maintenance and low cost, it’s ideal for people with busy lifestyles.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <button id="button9" type="button" class="btn" data-toggle="collapse" data-target="#collapse9" data-parent="#accordion"> <span class="glyphicon glyphicon-plus"></span></button>
                                        <h5 class="product-properties">Earthquake and Hurricane tested</h5>
                                    </div>
                                </div>
                                <div id="collapse9" class="panel-collapse collapse">
                                    <div class="panel-body">
                                            Build built with the Panel have been tested to resist earthquake magnitude of up to 9
                                    </div>
                                </div>
                            </div>-->
                            <div class="panel panel">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <button id="button10" type="button" class="btn" data-toggle="collapse" data-target="#collapse10" data-parent="#accordion"> <span class="glyphicon glyphicon-plus"></span></button>
                                        <h5 class="product-properties">Maximizes Internal Space</h5>
                                    </div>
                                </div>
                                <div id="collapse10" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="product-property-list">
                                            <li>The wall has excellent performance and takes up less space than the traditional wall, enlarging the effective internal space.</li>
                                            <li>Overall of 10% space saved in a 12sqm room</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <button id="button11" type="button" class="btn" data-toggle="collapse" data-target="#collapse11" data-parent="#accordion"> <span class="glyphicon glyphicon-plus"></span></button>
                                        <h5 class="product-properties">Energy Savings &amp; Heat Prevention</h5>
                                    </div>
                                </div>
                                <div id="collapse11" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="product-property-list">
                                            <li>Wall panel has a 0.3 [W/(m2.K)] heat index</li>
                                            <li>Compared to: Kiln fired hollow bricks or concrete block construction heat index is 0.5 - 0.6[W/(m2.K)]
</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <button id="button12" type="button" class="btn" data-toggle="collapse" data-target="#collapse12" data-parent="#accordion"> <span class="glyphicon glyphicon-plus"></span></button>
                                        <h5 class="product-properties">Durability &amp; Light Weight</h5>
                                    </div>
                                </div>
                                <div id="collapse12" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="product-property-list">
                                            <li>Wall panels weight less than 60 kg/m2</li>
                                            <li>Each panel (150*450*2800mm) can withstand a 328KN compressed load vertically.</li>
                                            <li>Low self-weight offers quake-proof property and reduces risk in earthquake zones.</li>
                                            <li>Compared to: Kiln fired hollow bricks or concrete block construction is 3 - 4 times the weight of the Wall panels</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a role="button" class="btn mt-30" href="products_kit.php" class="mb-20 mt-50">VIEW PRODUCTS</a> </div>

                </div>
                </div>
            <!-------end Product Properties --->
          <!-- FAQ -->
          
          <div class="tab-pane fade in" id="tab3">
            <div class="row">
              <div class="col-sm-12" id="panel2">
                <div class="panel-group" id="accordion1"> 
                  
                  <!--<a data-toggle="collapse" data-parent="#accordion" href="#collapse1"> -->

                  <div class="panel panel">
                    <div class="panel-heading">
                      <div class="panel-title">
                        <button id="button14" type="button" class="btn" data-toggle="collapse" data-target="#collapse14" data-parent="#accordion1"> <span class="glyphicon glyphicon-plus"></span></button>
                        <h5 class="product-properties">Q. How Do I Maintain the PHE Luxwood Panels</h5>
                      </div>
                    </div>
                    <div id="collapse14" class="panel-collapse collapse">
                      <div class="panel-body"> Ensure Panels are installed as per the manufacturer’s recommendations. There is no further maintenance required. </div>
                    </div>
                  </div>
                  <div class="panel panel">
                    <div class="panel-heading">
                      <div class="panel-title">
                        <button id="button15" type="button" class="btn" data-toggle="collapse" data-target="#collapse15" data-parent="#accordion1"> <span class="glyphicon glyphicon-plus"></span></button>
                        <h5 class="product-properties">Q. Does Condensation Form on the Inner Skin of Panels in Hot/Cold Condition</h5>
                      </div>
                    </div>
                    <div id="collapse15" class="panel-collapse collapse">
                      <div class="panel-body"> The Panel design prevents thermal bridging occurring between the outer & inner skins. If considerable temperature difference exists between the inside & outside, condensation will not occur. </div>
                    </div>
                  </div>
                  <div class="panel panel">
                    <div class="panel-heading">
                      <div class="panel-title">
                        <button id="button17" type="button" class="btn" data-toggle="collapse" data-target="#collapse17" data-parent="#accordion1"> <span class="glyphicon glyphicon-plus"></span></button>
                        <h5 class="product-properties">Q. CPEP Panel Warranty</h5>
                      </div>
                    </div>
                    <div id="collapse17" class="panel-collapse collapse">
                      <div class="panel-body"> Comprehensive manufacturer’s warranty is provided for all Panels up to 10 years. </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <!-- end of FAQ --> 
          
          <!-- Doors & Windows -->
          
          <div class="tab-pane fade in" id="tab4">
            <h3 class="product-use">PHE Luxwood Panel Composite Doors</h3>
            <ul class="product-property-list">
              <li>Waterproof</li>
              <li>Pest proof</li>
              <li>Strong and long lasting</li>
              <li>No shrinkage or swelling</li>
              <li>Various patterns and styles</li>
              <li>As strong as solid wood door</li>
            </ul>
            <br>
            <br>
            <h3 class="product-use">INCLUSIONS</h3>
            <ul class="product-property-list">
              <li>External and Internal wall panels including cut outs for doors and windows</li>
              <li>Top and bottom channels including fixings</li>
              <li>Windows and glass sliding doors</li>
              <li>Solid Timber external doors including architraves</li>
              <li>Hollow core internal doors including architraves</li>
              <li>Skirtings</li>
              <li>Roof panels</li>
              <li>Gutter, fascia and barge capping</li>
            </ul>
            <br>
            <br>
            <h3 class="product-use">Windows</h3>
            <ul class="product-property-list">
              <li>Sourced from outside supplier in Plastic or Aluminium</li>
              <li>Good heat insulation performance</li>
              <li>Excellent air proof and waterproof property</li>
              <li>Window pane is Argon filled double-glazed to improve heat and sound insulation of entire structure</li>
              <li>Constructed to NZSAS standards</li>
            </ul>
            <br>
            <br>
            <h3 class="product-use">INTERNAL INCLUSIONS</h3>
            <ul class="product-property-list">
              <li>LED lighting</li>
              <li>Bathtub</li>
              <li>Washbasin/Vanity</li>
              <li>Toilet</li>
              <li>Taps and shower unit</li>
              <li>Kitchen cabinets</li>
              <li>Kitchen sink and tap unit</li>
              <li>Vinyl floor coverings</li>
            </ul>
            <br>
          </div>
          
          <!-- end of Doors & Windows --> 
          
        </div>
      </div>
    </div>
  </div>
  </div>
  <script>
    $(function(){
        $('.panel-collapse').on('hide.bs.collapse', function () {
            var currentId = $(this).attr('id');
            var numb = currentId.match(/\d+$/)[0];
            var buttonId="#button".concat(numb);
            $(buttonId).html('<span class="glyphicon glyphicon-plus"></span>');
        })
        $('.panel-collapse').on('show.bs.collapse', function () {
            var currentId = $(this).attr('id');
            var numb = currentId.match(/\d+$/)[0];
            var buttonId="#button".concat(numb);
            $(buttonId).html('<span class="glyphicon glyphicon-minus"></span>');
        })
    })


</script>
  <?php include 'inc/highlights.php';?>
  <?php include 'inc/services.php';?>
  <?php include 'inc/footer.php';?>
