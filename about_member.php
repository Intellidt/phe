<?php include 'inc/nav.php';  ?>

  <div class="container-fluid banner-top banner-aboutus">
    <div class="title-container container">
      <h1 style="color:white">THE LUXWOOD FAMILY</h1>
    </div>
  </div>
  
  <div class="container-fluid content-wrapper" >
  <div class="container">
    
    <!-- core member starts -->
    
    <div class="row">
      <div class="col-sm-12">
        <p>As a multi-faceted system, Luxwood Panel Building System values its core members, ambassadors, facilitators, and every family that purchases a Luxwood home. As every Luxwood home needs specific components to make the system work, so does our company structure, where each component is crucial in our success. Our core members represent the foundation of the company bringing with them their knowledge and experience.</p>
        <p class="mb-50">As we all know a foundation is nothing without its walls, our company is built with strong family values which adds strength and structure. The Luxwood Ambassadors and homeowners are the roof and the structure of our home. They represent the level of protection we need to deliver in order to keep the cold and rain at bay. Every Luxwood family members plays a vital role, in making our ‘houses’ homes. We are more than just four walls and a roof, we are family building a future filled with homes that instilling a sense of security, comfort and peacefulness that we believe every human being deserves. </p>

        <div class="title-block" >
          <h5>The Luxwood Family</h5>
          <h1>Core Members</h1>
        </div>
      </div>
    </div>
    
    
    <div class="row mb-50">
      <div class="col-sm-3 adv-tile">
        <div class="img-header"><img src="images/about/about_core_member_1.jpg" width="150" height="150"></div>
        <p style="text-align:center"><strong>Mark</strong><BR>
          <em>Director/owner</em></p>
        <p style="text-align:justify">Mark is the owner and creator of the whole operation. With over 20 years of experience, there is very little Mark cannot do.</p>
      </div>
      <div class="col-sm-3">
        <div class="img-header"><img src="images/about/about_core_member_2.jpg" width="150" height="150"></div>
        <p style="text-align:center"><strong>Claire</strong><BR>
          <em>Manager</em></p>
        <p style="text-align:justify">Claire is hard -working, dedicated and kind; the glue that keeps our Cape town branch going.</p>
      </div>
      <div class="col-sm-3">
        <div class="img-header"><img src="images/about/about_core_member_3.jpg" width="150" height="150"></div>
        <p style="text-align:center"><strong>Annie</strong><BR>
          <em>General Manager</em></p>
        <p style="text-align:justify">Annie is an enthusiastic, diligent woman who keeps our China factory together.</p>
      </div>
      <div class="col-sm-3">
        <div class="img-header"><img src="images/about/about_core_member_4.jpg" width="150" height="150"></div>
        <p style="text-align:center"><strong>Petra</strong><BR>
          <em>General Manager</em></p>
        <p style="text-align:justify">Petra is an enterprising, and dedicated woman with many years of international experience.</p>
      </div>
    
    </div>
    
    <!-- core member ends --> 
    
    <!-- Stakeholders starts -->
    
    <div class="row">
    <div class="col-sm-12">
      <div class="title-block" >
        <h5>The Luxwood Family</h5>
        <h1>Luxwood Stakeholders</h1>
      </div>
      </div>
      </div>
      
     <div class="row mb-50"> 
      <div class="col-sm-3 adv-tile">
        <div class="img-header"><img src="images/about/about_stakeholders_1.jpg" width="150" height="150"></div>
        <p style="text-align:center"><strong>Chong</strong></p>
        <p style="text-align:justify">Chong is currently sitting on the board of Trust Waikato and quite active in the local community. Chong is the main facilitator in New Zealand for Luxwood Homes.</p>
      </div>
      <div class="col-sm-3">
        <div class="img-header"><img src="images/about/about_stakeholders_2.jpg" width="150" height="150"></div>
        <p style="text-align:center"><strong>Pieter</strong></p>
        <p style="text-align:justify">Pieter is chairman of the Law Alliance NZ (LANZ), with member firms located throughout New Zealand providing specialist legal services in almost every aspect of the law.</p>
      </div>
    </div>

    
    <!-- Stakeholders ends --> 
    
    <!-- Facilitators starts -->
    
    <div class="row">
    <div class="col-sm-12">
      <div class="title-block" >
        <h5>The Luxwood Family</h5>
        <h1>Luxwood Facilitators</h1>
      </div>
     </div>
     </div>
      
     <div class="row mb-50"> 
      <div class="col-sm-3 adv-tile">
        <div class="img-header"><img src="images/about/about_facilitators_1.jpg" width="150" height="150"></div>
        <p style="text-align:center"><strong>Amber-leigh</strong></p>
        <p style="text-align:justify">Amber-Leigh’s big smile is probably the first thing you will see walking into our door, with her enthusiastic nature she handles our Web Development.</p>
      </div>
      <div class="col-sm-3">
        <div class="img-header"><img src="images/about/about_facilitators_2.jpg" width="150" height="150"></div>
        <p style="text-align:center"><strong>Mokhele</strong></p>
        <p style="text-align:justify">Mokhele is a passionate, tireless perfectionist with a great eye for detail and is always ready for any task he may encounter.</p>
      </div>
      <div class="col-sm-3">
        <div class="img-header"><img src="images/about/about_facilitators_3.jpg" width="150" height="150"></div>
        <p style="text-align:center"><strong>Michael</strong></p>
        <p style="text-align:justify">Micheal is a dedicated, optimistic young man, whose forklift driving skills leave us is awe.</p>
      </div>
      <div class="col-sm-3">
        <div class="img-header"><img src="images/about/about_facilitators_4.jpg" width="150" height="150"></div>
        <p style="text-align:center"><strong>Guy</strong></p>
        <p style="text-align:justify">Guy is a member of our Luxwood USA Advisory board, He holds many years of international experience in leading large organizations</p>
      </div>
    </div>

    <div class="row mb-50">
      <div class="col-sm-3 adv-tile">
        <div class="img-header"><img src="images/about/about_facilitators_5.jpg" width="150" height="150"></div>
        <p style="text-align:center"><strong>Jody</strong></p>
        <p style="text-align:justify">Jody is an intuitive, hard-working young lady who we are proud to say is one of our Technologists. She attained her National diploma through the Cape Peninsula University of Technology.</p>
      </div>
      <div class="col-sm-3">
        <div class="img-header"><img src="images/about/about_facilitators_6.jpg" width="150" height="150"></div>
        <p style="text-align:center"><strong>Nicholas</strong></p>
        <p style="text-align:justify">Nicholas is our local engineer. He studied Civil Engineering and has a wide range of experience. We are Proud to say that he is a part of the Luxwood family.</p>
      </div>
      <div class="col-sm-3">
        <div class="img-header"><img src="images/about/about_facilitators_7.jpg" width="150" height="150"></div>
        <p style="text-align:center"><strong>Graeme</strong></p>
        <p style="text-align:justify">Graeme is a certified APEC member. He is our global engineer and attained his Bachelor of Engineering at the NSW Institute of Technology.</p>
      </div>
      <div class="col-sm-3">
        <div class="img-header"><img src="images/about/about_facilitators_8.jpg" width="150" height="150"></div>
        <p style="text-align:center"><strong>John</strong></p>
        <p style="text-align:justify">John works with the Luxwood team in a sales capacity.  He has a wealth of experience working with College Campuses, Hospitals and B&amp;I.</p>
      </div>
    </div>


    <div class="row mb-50">
      <div class="col-sm-3 adv-tile">
        <div class="img-header"><img src="images/about/about_facilitators_9.jpg" width="150" height="150"></div>
        <p style="text-align:center"><strong>Amy</strong></p>
        <p style="text-align:justify">Amy is an innovative, diligent and enthusiastic young lady who we are proud to say is one of our Architectural draughtsmen. She has attained years of invaluable experience in the area.</p>
      </div>
      <div class="col-sm-3">
        <div class="img-header"><img src="images/about/about_facilitators_10.jpg" width="150" height="150"></div>
        <p style="text-align:center"><strong>Paul</strong></p>
        <p style="text-align:justify">With an education in Computer Science, Business Administration and Project Management Paul’s entrepreneur vision founded Intelli Group, a diversified international business with Global partners.</p>
      </div>
    </div>
    
    <!-- Facilitators ends --> 
    
  </div>
  </div>
  
  
<?php include 'inc/services.php';?>
<?php include 'inc/footer_contact.php';?>
<?php include 'inc/footer.php';?>
