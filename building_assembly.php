<?php include 'inc/nav.php';  ?>

  <div class="container-fluid banner-top banner-build-assembly">
    <div class="title-container container">
      <h1 style="color:white">BUILDING SYSTEM</h1>
    </div>
  </div>
  
  <div class="container-fluid content-wrapper">
      <div class="container">
  
      <div class="row mb-50">
        <div class="col-sm-12">
          <div class="title-block mb-20">
            <h5>BUILDING APPLICATION</h5>
            <h1>Assembly</h1>
            <BR>
          </div>
          <h3 class="mb-0"> single unit with two bedrooms can be completed by a team of 8 Luxwood trained installers.<BR>
          <span class="dark-red">AFTER 24 HOURS</span> The house is ready for optional external &amp; internal finishing.</h3>
            
        </div>
      </div>
      
      <div class="row mb-50">
          <div class="col-md-8">
              <div class="row">
                  <div class="col-sm-6 building-system-img">

                      <div class="step"> <img src="images/building_system/icon_assembly_1.png" alt="PHE Building System Assembly" class="img-responsive" width="120" height="120">
                          <div class="step-operation">
                              <p>Hour 0</p>
                              <p>Concrete Slab Prepared </p>
                          </div>
                      </div>

                      <div class="verticalLine"></div>

                      <div class="step"> <img src="images/building_system/icon_assembly_2.png" alt="" class="img-responsive" width="120" height="120">
                          <div class="step-operation">
                              <p>Hour 1</p>
                              <p>Deliverd to Site </p>
                          </div>
                      </div>

                      <div class="verticalLine"></div>

                      <div class="step"><img src="images/building_system/icon_assembly_3.png" alt="" class="img-responsive" width="120" height="120">
                          <div class="step-operation">
                              <p>Hour 3</p>
                              <p>Floor Panels &amp; Posts Installed </p>
                          </div>
                      </div>

                      <div class="verticalLine"></div>

                      <div class="step"> <img src="images/building_system/icon_assembly_4.png" alt="" class="img-responsive" width="120" height="120">
                          <div class="step-operation">
                              <p>Hour 5</p>
                              <p>Wall Panels, Windows Frames Installed </p>
                          </div>
                      </div>

                  </div>
                  <div class="col-sm-6 building-system-img">

                      <div class="step"><img src="images/building_system/icon_assembly_5.png" alt="" class="img-responsive" width="120" height="120">
                          <div class="step-operation">
                              <p>Hour 6</p>
                              <p>Steel Trust Installed </p>
                          </div>
                      </div>

                      <div class="verticalLine"></div>

                      <div class="step"><img src="images/building_system/icon_assembly_6.png" alt="" class="img-responsive" width="120" height="120">
                          <div class="step-operation">
                              <p>Hour 8</p>
                              <p>Windows &amp; Doors Installed </p>
                          </div>
                      </div>

                      <div class="verticalLine"></div>

                      <div class="step"><img src="images/building_system/icon_assembly_7.png" alt="" class="img-responsive" width="120" height="120">
                          <div class="step-operation">
                              <p>Hour 14</p>
                              <p>Roof System Installed </p>
                          </div>
                      </div>

                  </div>
              </div>
          </div>

        

        
        <div class="col-md-4 building-system-img">
          <video class="video-player" width="100%" height="300" frameborder="0" controls>
            <source src="images/video/1day.mp4" type="video/mp4">
            <source src="images/video/1day.ogg" type="video/ogg">
            Your browser does not support the video tag. </video>
        </div>
        
        
      </div><!--- end of row-->
      </div>
</div>

  <?php include 'inc/services.php';?>
  <?php include 'inc/footer.php';?>
