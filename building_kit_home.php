 <?php include 'inc/nav.php';  ?>

<div class="container-fluid banner-top banner-build-kit">
  <div class="title-container container">
    <h1 style="color:white">BUILDING SYSTEM</h1>
  </div>
</div>

<div class="container-fluid content-wrapper">
    <div class="container">

	<div class="row">
    <div class="col-sm-12">
    <div class="title-block" >
    <h5>BUILDING APPLICATION</h5>
    <h1> PHE Luxwood Kit Homes</h1>
    </div>
    </div>
    </div>
        <div class="row">
        <div class="col-sm-4 building-system-img">
        <img src="images/building_system/kit_home.jpg" alt="PHE Building System Kit Home" class="img-responsive" width="300" height="300">
        </div>
        <div class="col-sm-8 building-system-content">
          <p>Luxwood Kit Homes are the ingenious fusion of combining both our innovative products with our building system. Offering a solution for affordable housing by using improved technology without sacrificing the structure. Here at Luxwood we proved an array of pre-designed structures of our Kit Homes as well as offering our expertise to help you customize your desired home.</p>
          <p>Luxwood Kit Homes is proven to be cost-effective, innovative and environment friendly. In fact, the design of the homes are constructed with a focus on safety, stability, durability, fire and earthquake resistant.</p>
        </div>
        </div>
    
  </div>
</div>


<?php include 'inc/services.php';?>
<?php include 'inc/footer.php';?>


     