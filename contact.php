<?php include 'inc/nav.php';  ?>
<div class="container-fluid banner-top banner-contact">
  <div class="title-container container">
    <h1 style="color:white">CONTACT</h1>
  </div>
</div>

<!-- the section of the contact table and form -->
<div class="container-fluid">
     <div class="container contactus-wrapper">
     
      <div class="row mb-30">
		<div class="col-sm-12 mb-30">
		<p style="text-align:center">If you have any questions about our services, products or any general enquiries, feel free to send us a message via the contact form to the right.
		</p>
		</div>
        
        <div class="row">
          <div class="col-sm-5 contact-detail">
			<div class="address">
            <p><span class="dark-red"><strong>Luxwood Homes New Zealand</strong></span> <BR>
            33 Harwood Street Hamilton Central,<BR>Hamilton New Zealand 3204</p>
			</div>
			<div class="phone mb-30">
			<span class="glyphicon glyphicon-earphone"></span><p> +64 078583628</p>
			</div>
            
            <div class="address">
                <p><span class="dark-red"><strong>PHE Luxwood Homes South Africa</strong></span><BR>
            Unit 10 Montague Square,<BR>28 Montague Drive Montague Gardens Cape Town</p>
			</div>
			<div class="phone mb-30">
			<span class="glyphicon glyphicon-earphone"></span><p>+27 21 555 3226</p>
			</div>
            
            <div class="address">
                <p><span class="dark-red"><strong>PHE Solutions China</strong></span><BR>
            7 Huaquan First Road Longquan Town,<BR>Jimo City Qingdao, China</p>
			</div>
			<div class="phone">
			<span class="glyphicon glyphicon-earphone"></span><p>+61 756463045</p>
			</div>
          </div>
          <!-- end of contact information --> 
          
          <!-- the section of get in touch -->
          <div class="col-sm-7">
          
          <!-- the contact form -->
          <div class="error" style="color: red"></div>
          <form method="POST" id="contact-form">
            <div class="row">
              <div class="form-group col-lg-6">
                <input name="name" type="text" class="form-control" placeholder="Your Name *">
              </div>
              <div class="form-group col-lg-6">
                <input name="email" type="text" class="form-control" placeholder="Your E-Mail *">
              </div>
            <BR>
            
              <div class="form-group col-lg-6">
                <input name="phone" type="text" class="form-control" placeholder="Phone Number *">
              </div>
              <div class="form-group col-lg-6">
                <input name="subject" type="text" class="form-control" placeholder="Subject*">
              </div>
            <BR>
            
              <div class="form-group col-lg-12">
                <textarea class="form-control" name="message" style="line-height: 100px;" placeholder="Write your message..."></textarea>
              </div>
            <BR>
            
              <div class="form-group col-lg-2"> <a class="form-control" id="contact-form-submit" type="submit">Submit </a> </div>
            </div>
            <br>
          </form>
          <div class="success"></div>
        </div>
        <!-- end of the get in touch --> 
        
      </div>
    </div>
  </div>
  </div>



<!-- for the form submitted --> 
<script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> 
<script>
$(document).ready(function(){
  $('#contact-form-submit').click(function(){
    var str = $('#contact-form').serialize();
    $.ajax({
      type: "POST",
      url: "mail.php",
      data: str,
      success: function(msg){
           if(msg == 'OK'){
             result = '<span class="success">Your message has been successfully sent. Thank you!</span>';
             $('#contact-form').fadeOut('slow');
           }else{
             result = msg;
           }
           $('.error').html(result);
      }
      });
  });
});
</script>

<?php include 'inc/services.php';?>
<?php include 'inc/footer.php';?>