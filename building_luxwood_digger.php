<?php include 'inc/nav.php';  ?>


<div class="container-fluid banner-top banner-build-luxwood">
    <div class="container title-container">
      <h1 style="color:white">BUILDING SYSTEM</h1>
    </div>
  </div>
  
<div class="container-fluid content-wrapper">
    <div class="container">

      <div class="row">
      
        <div class="col-sm-12">
          <div class="title-block" >
            <h5>BUILDING APPLICATION</h5>
            <h1>Luxwood Digger</h1>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-sm-4 building-system-img"> <img src="images/building_system/luxwood_digger.jpg" alt="PHE Building System Luxwood Digger" class="img-responsive" width="300" height="300"> </div>
        <div class="col-sm-8 building-system-content">
          <p>The Digger Machine is a wheel style mini skid loader designed for compact construction work.  It has a quick attachment mount plate which makes it easy for an operator to connect different attachments.  It is designed for operation in temperatures typically experienced in earth moving and construction work environments.</p>
        </div>
      </div>
    </div>
</div>


<?php include 'inc/services.php';?>
<?php include 'inc/footer.php';?>