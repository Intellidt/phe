<?php include 'inc/nav.php';  ?>
  <div class="container-fluid banner-top banner-project">
    <div class="title-container container">
      <h1 style="color:white">PROJECTS</h1>
    </div>
  </div>
  <div class="container-fluid">
    <div class="container content-wrapper">
      <div class="row">
        <div class="col-sm-12">
          <div class="title-block" >
            <h5>BUILDING APPLICATION</h5>
            <h1>Projects</h1>
          </div>
        </div>
      </div>
      
      <!-- MAIN (Center website) -->
      
      <div id="myBtnContainer">
      <a class="link active" onclick="filterSelection('all')">Show all</a>
      <a class="link" onclick="filterSelection('australia_1')">Coomera</a>
      <a class="link" onclick="filterSelection('australia_2')">Drift House</a>
      <a class="link" onclick="filterSelection('australia_3')">Heron</a>
      <a class="link" onclick="filterSelection('australia_4')">Pacific</a>
      <a class="link" onclick="filterSelection('australia_5')">Sydney House</a>
      <a class="link" onclick="filterSelection('australia_6')">Warwick Farm</a>
      <a class="link" onclick="filterSelection('australia_7')">Ballara Home</a>
      <a class="link" onclick="filterSelection('australia_8')">Grandview House</a>  
      <a class="link" onclick="filterSelection('s_africa_1')">Nerang House</a>
      <a class="link" onclick="filterSelection('s_africa_2')">Theodor Herzl School</a>
      
      </div>

        <select id="selectmenu" onchange="filterSelection(this.value)">
            <option value="all">Show all</option>
            <option value="australia_1">Coomera</option>
            <option value="australia_2">Drift House</option>
            <option value="australia_3">Heron</option>
            <option value="australia_4">Pacific</option>
            <option value="australia_5">Sydney House</option>
            <option value="australia_6">Warwick Farm</option>
            <option value="australia_7">Ballara Home</option>
            <option value="australia_8">Grandview House</option>
            <option value="s_africa_1">Nerang House</option>
            <option value="s_africa_2">Theodor Herzl School</option>
            
        </select>
      <!-- Portfolio Gallery Grid -->
      
      <div class="row">
      
      <!-- Australia starts -->
      
        <div class="col-sm-4 column australia_1 content">
        <img src="images/projects/australia/phe_projects_australia_1a.jpg" alt="PHE projects Coomera" style="width:100%"></div>
        <div class="col-sm-4 column australia_1 content">
        <img src="images/projects/australia/phe_projects_australia_1b.jpg" alt="PHE projects Coomera" style="width:100%"></div>
        <div class="col-sm-4 column australia_1 content">
        <img src="images/projects/australia/phe_projects_australia_1c.jpg" alt="PHE projects Coomera" style="width:100%"></div>
        <div class="col-sm-4 column australia_1 content">
        <img src="images/projects/australia/phe_projects_australia_1d.jpg" alt="PHE projects Coomera" style="width:100%"></div>
        <div class="col-sm-4 column australia_1 content">
        <img src="images/projects/australia/phe_projects_australia_1e.jpg" alt="PHE projects Coomera" style="width:100%"></div>
        <div class="col-sm-4 column australia_1 content">
        <img src="images/projects/australia/phe_projects_australia_1f.jpg" alt="PHE projects Coomera" style="width:100%"></div>
        <div class="col-sm-4 column australia_1 content">
        <img src="images/projects/australia/phe_projects_australia_1g.jpg" alt="PHE projects Coomera" style="width:100%"></div>
        <div class="col-sm-4 column australia_1 content">
        <img src="images/projects/australia/phe_projects_australia_1h.jpg" alt="PHE projects Coomera" style="width:100%"></div>
             
        <div class="col-sm-4 column australia_2 content">
        <img src="images/projects/australia/phe_projects_australia_2a.jpg" alt="PHE projects Heron" style="width:100%"></div>
        <div class="col-sm-4 column australia_2 content">
        <img src="images/projects/australia/phe_projects_australia_2b.jpg" alt="PHE projects Heron" style="width:100%"></div>
        <div class="col-sm-4 column australia_2 content">
        <img src="images/projects/australia/phe_projects_australia_2c.jpg" alt="PHE projects Heron" style="width:100%"></div>
        <div class="col-sm-4 column australia_2 content">
        <img src="images/projects/australia/phe_projects_australia_2d.jpg" alt="PHE projects Heron" style="width:100%"></div>
        
        <div class="col-sm-4 column australia_3 content">
        <img src="images/projects/australia/phe_projects_australia_3a.jpg" alt="PHE projects Heron" style="width:100%"></div>
        <div class="col-sm-4 column australia_3 content">
        <img src="images/projects/australia/phe_projects_australia_3b.jpg" alt="PHE projects Heron" style="width:100%"></div>
        <div class="col-sm-4 column australia_3 content">
        <img src="images/projects/australia/phe_projects_australia_3c.jpg" alt="PHE projects Heron" style="width:100%"></div>
        <div class="col-sm-4 column australia_3 content">
        <img src="images/projects/australia/phe_projects_australia_3d.jpg" alt="PHE projects Heron" style="width:100%"></div>
        <div class="col-sm-4 column australia_3 content">
        <img src="images/projects/australia/phe_projects_australia_3e.jpg" alt="PHE projects Heron" style="width:100%"></div>
        
        <div class="col-sm-4 column australia_4 content">
        <img src="images/projects/australia/phe_projects_australia_4a.jpg" alt="PHE projects Pacific" style="width:100%"></div>
        <div class="col-sm-4 column australia_4 content">
        <img src="images/projects/australia/phe_projects_australia_4b.jpg" alt="PHE projects Pacific" style="width:100%"></div>
        <div class="col-sm-4 column australia_4 content">
        <img src="images/projects/australia/phe_projects_australia_4c.jpg" alt="PHE projects Pacific" style="width:100%"></div>
        <div class="col-sm-4 column australia_4 content">
        <img src="images/projects/australia/phe_projects_australia_4d.jpg" alt="PHE projects Pacific" style="width:100%"></div>
        <div class="col-sm-4 column australia_4 content">
        <img src="images/projects/australia/phe_projects_australia_4e.jpg" alt="PHE projects Pacific" style="width:100%"></div>
        <div class="col-sm-4 column australia_4 content">
        <img src="images/projects/australia/phe_projects_australia_4f.jpg" alt="PHE projects Pacific" style="width:100%"></div>
        
        <div class="col-sm-4 column australia_5 content">
        <img src="images/projects/australia/phe_projects_australia_5a.jpg" alt="PHE projects Sydney House" style="width:100%"></div>
        <div class="col-sm-4 column australia_5 content">
        <img src="images/projects/australia/phe_projects_australia_5b.jpg" alt="PHE projects Sydney House" style="width:100%"></div>
        <div class="col-sm-4 column australia_5 content">
        <img src="images/projects/australia/phe_projects_australia_5c.jpg" alt="PHE projects Sydney House" style="width:100%"></div>
        <div class="col-sm-4 column australia_5 content">
        <img src="images/projects/australia/phe_projects_australia_5d.jpg" alt="PHE projects Sydney House" style="width:100%"></div>
        
        <div class="col-sm-4 column australia_6 content">
        <img src="images/projects/australia/phe_projects_australia_6a.jpg" alt="PHE projects Warwick Farm" style="width:100%"></div>
        <div class="col-sm-4 column australia_6 content">
        <img src="images/projects/australia/phe_projects_australia_6b.jpg" alt="PHE projects Warwick Farm" style="width:100%"></div>
        <div class="col-sm-4 column australia_6 content">
        <img src="images/projects/australia/phe_projects_australia_6c.jpg" alt="PHE projects Warwick Farm" style="width:100%"></div>
        <div class="col-sm-4 column australia_6 content">
        <img src="images/projects/australia/phe_projects_australia_6d.jpg" alt="PHE projects Warwick Farm" style="width:100%"></div>
        <div class="col-sm-4 column australia_6 content">
        <img src="images/projects/australia/phe_projects_australia_6e.jpg" alt="PHE projects Warwick Farm" style="width:100%"></div>
        
        <div class="col-sm-4 column australia_7 content">
        <img src="images/projects/australia/phe_projects_australia_7a.jpg" alt="PHE projects Warwick Farm" style="width:100%"></div>
        <div class="col-sm-4 column australia_7 content">
        <img src="images/projects/australia/phe_projects_australia_7b.jpg" alt="PHE projects Warwick Farm" style="width:100%"></div>
        <div class="col-sm-4 column australia_7 content">
        <img src="images/projects/australia/phe_projects_australia_7c.jpg" alt="PHE projects Warwick Farm" style="width:100%"></div>
        <div class="col-sm-4 column australia_7 content">
        <img src="images/projects/australia/phe_projects_australia_7d.jpg" alt="PHE projects Warwick Farm" style="width:100%"></div>
        <div class="col-sm-4 column australia_7 content">
        <img src="images/projects/australia/phe_projects_australia_7e.jpg" alt="PHE projects Warwick Farm" style="width:100%"></div>
        <div class="col-sm-4 column australia_7 content">
        <img src="images/projects/australia/phe_projects_australia_7f.jpg" alt="PHE projects Warwick Farm" style="width:100%"></div>
        
        <div class="col-sm-4 column australia_8 content">
        <img src="images/projects/australia/phe_projects_australia_8a.jpg" alt="PHE projects Warwick Farm" style="width:100%"></div>
        <div class="col-sm-4 column australia_8 content">
        <img src="images/projects/australia/phe_projects_australia_8b.jpg" alt="PHE projects Warwick Farm" style="width:100%"></div>
        <div class="col-sm-4 column australia_8 content">
        <img src="images/projects/australia/phe_projects_australia_8c.jpg" alt="PHE projects Warwick Farm" style="width:100%"></div>
        <div class="col-sm-4 column australia_8 content">
        <img src="images/projects/australia/phe_projects_australia_8d.jpg" alt="PHE projects Warwick Farm" style="width:100%"></div>
        <div class="col-sm-4 column australia_8 content">
        <img src="images/projects/australia/phe_projects_australia_8e.jpg" alt="PHE projects Warwick Farm" style="width:100%"></div>
        <div class="col-sm-4 column australia_8 content">
        <img src="images/projects/australia/phe_projects_australia_8f.jpg" alt="PHE projects Warwick Farm" style="width:100%"></div>
        
        <!-- Australia ends -->
        
        
        <!-- S Africa starts -->
        
        
        
        <div class="col-sm-4 column s_africa_1 content">
        <img src="images/projects/s_africa/phe_projects_s_africa_1a.jpg" alt="PHE projects Nerang House" style="width:100%"></div>
        <div class="col-sm-4 column s_africa_1 content">
        <img src="images/projects/s_africa/phe_projects_s_africa_1b.jpg" alt="PHE projects Nerang House" style="width:100%"></div>
        <div class="col-sm-4 column s_africa_1 content">
        <img src="images/projects/s_africa/phe_projects_s_africa_1c.jpg" alt="PHE projects Nerang House" style="width:100%"></div>
        <div class="col-sm-4 column s_africa_1 content">
        <img src="images/projects/s_africa/phe_projects_s_africa_1d.jpg" alt="PHE projects Nerang House" style="width:100%"></div>
        
        <div class="col-sm-4 column s_africa_2 content">
        <img src="images/projects/s_africa/phe_projects_s_africa_2a.jpg" alt="PHE projects Theodor Herzel School" style="width:100%"></div>
        <div class="col-sm-4 column s_africa_2 content">
        <img src="images/projects/s_africa/phe_projects_s_africa_2b.jpg" alt="PHE projects Theodor Herzel School" style="width:100%"></div>
        <div class="col-sm-4 column s_africa_2 content">
        <img src="images/projects/s_africa/phe_projects_s_africa_2c.jpg" alt="PHE projects Theodor Herzel School" style="width:100%"></div>
        <div class="col-sm-4 column s_africa_2 content">
        <img src="images/projects/s_africa/phe_projects_s_africa_2d.jpg" alt="PHE projects Theodor Herzel School" style="width:100%"></div>
        <div class="col-sm-4 column s_africa_2 content">
        <img src="images/projects/s_africa/phe_projects_s_africa_2e.jpg" alt="PHE projects Theodor Herzel School" style="width:100%"></div>
        <div class="col-sm-4 column s_africa_2 content">
        <img src="images/projects/s_africa/phe_projects_s_africa_2f.jpg" alt="PHE projects Theodor Herzel School" style="width:100%"></div>
        
        

         
        <!-- S Africa starts -->
        
         
      </div><!-- END GRID -->

 <!-- END MAIN --> 
    </div>
    
    
    <script>
filterSelection("all")
function filterSelection(c) {
  var x, i;
  x = document.getElementsByClassName("column");
  if (c == "all") c = "";
  for (i = 0; i < x.length; i++) {
    w3RemoveClass(x[i], "show");
    if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
  }
}

function w3AddClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) == -1) {element.className += " " + arr2[i];}
  }
}

function w3RemoveClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1);     
    }
  }
  element.className = arr1.join(" ");
}


// Add active class to the current button (highlight it)
var btnContainer = document.getElementById("myBtnContainer");
var btns = btnContainer.getElementsByClassName("link");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function(){
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}
</script> 
  </div>
</div>

<?php include 'inc/highlights.php';?>
<?php include 'inc/services.php';?>
<?php include 'inc/footer_contact.php';?>
<?php include 'inc/footer.php';?>
