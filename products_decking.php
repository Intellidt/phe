<?php include 'inc/nav.php';  ?>

<div class="container-fluid banner-top banner-products-decking">
  <div class="title-container container">
    <h1 style="color:white">PRODUCTS</h1>
  </div>
</div>

<div class="container-fluid">
  <div class="container content-wrapper">
  
    <div class="row">
      <div class="col-sm-12">
        <div class="title-block" >
          <h5>AWESOME PRODUCT</h5>
          <h1>PHE Luxwood Decking</h1>
        </div>
      </div>
    </div>
    
      <!-- 3D Decking -->
  
    <div class="row mb-50">
    
      <div class="col-sm-6">
        <p>The Luxwood Decking has a unique closed surface technology when compared to traditional
composite or wood decks. Through scientific research and experiment, we have finally created
an outstanding surface one such can be described as artwork. By laying large tracks of land, the
3D embossing surface provides beautiful decoration, and guarantee the skid resistant function.</p>

<UL>
<li><strong>Product Features:</strong></li>
<li>• Superior stain resistance</li>
<li>• The surface is 3D embossed wood grain design</li>
<li>• The emboss will not fray or turn shallow as time goes by</li>
<li>• Strong solid wood texture, like a carefully carved artwork.</li>
</ul>

      </div>
      
      <div class="col-sm-6">
          <div id="productCarousel" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators carousel-indicators-product">
                  <li data-target="#productCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#productCarousel" data-slide-to="1"></li>
                  <li data-target="#productCarousel" data-slide-to="2"></li>
                  <li data-target="#productCarousel" data-slide-to="3"></li>
                  <li data-target="#productCarousel" data-slide-to="4"></li>
                  <li data-target="#productCarousel" data-slide-to="5"></li>
                  <li data-target="#productCarousel" data-slide-to="6"></li>
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                  <div class="item active">
                      <img src="images/products/decking/product_3d_decking_1.jpg" alt="">
                      <div class="carousel-product">
                          <p>TW-K03 (150 x 25mm)</p>
                      </div>
                  </div>

                  <div class="item">
                      <img src="images/products/decking/product_3d_decking_2.jpg" alt="">
                      <div class="carousel-product">
                          <p>TS-04 (200 x 25mm) &nbsp; &nbsp; | &nbsp; &nbsp; TS-05 (200 x 25mm) </p>
                      </div>
                  </div>

                  <div class="item">
                      <img src="images/products/decking/product_3d_decking_3.jpg" alt="">
                      <div class="carousel-product">
                          <p>TS-03 (150 x 25mm)</p>
                      </div>
                  </div>

                  <div class="item">
                      <img src="images/products/decking/product_3d_decking_4.jpg" alt="">
                      <div class="carousel-product">
                          <p>TH-16 (134 x 24mm)</p>
                      </div>
                  </div>
                  <div class="item">
                      <img src="images/products/decking/product_3d_decking_5.jpg" alt="">
                      <div class="carousel-product">
                          <p>TH-09B (134 x 24mm)</p>
                      </div>
                  </div>
                  <div class="item">
                      <img src="images/products/decking/product_3d_decking_6.jpg" alt="">
                      <div class="carousel-product">
                          <p>TH-05 (139 x 9mm)</p>
                      </div>
                  </div>
                  <div class="item">
                      <img src="images/products/decking/product_3d_decking_7.jpg" alt="">
                      <div class="carousel-product">
                          <p>CD-01 (140 x 21mm)</p>
                      </div>
                  </div>
              </div>
          </div>
      </div><!--/col-->
</div><!--/row-->

</div>
</div>


<!--- color decking --->

<div class="container-fluid color-display-decking">
  <div class="container content-wrapper">
    <div class="row">
      <div class="col-sm-3">
        <div class="title-block">
          <h5>DECKING </h5>
          <h1 style="color:white;">Color Display</h1>
        </div>
      </div>
      <div class="col-sm-9">
        <ul class="highlights-list color-img">
          <li> <img src="images/products/decking/decking_color_display_1.png" alt="" class="img-responsive" />
            <h4> Grey Walnut</h4>
          </li>
          <li> <img src="images/products/decking/decking_color_display_2.png" alt="" class="img-responsive" />
            <h4>Brown Wood</h4>
          </li>
          <li> <img src="images/products/decking/decking_color_display_3.png" alt="" class="img-responsive" />
            <h4>Red Wood</h4>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>

<!--- end of color decking --->


<div class="container-fluid">
  <div class="container content-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <div class="title-block" >
          <h5>DECKING</h5>
          <h1> Accessories Display </h1>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3 adv-tile">
        <div class="img-header accessories"><img src="images/products/decking/decking_accessories_1.jpg" alt="Floor" />
          <h4>Floor</h4>
        </div>
      </div>
      <div class="col-sm-3 adv-tile">
        <div class="img-header accessories"><img src="images/products/decking/decking_accessories_2.jpg" alt="Skeleton" />
          <h4>Skeleton</h4>
        </div>
      </div>
      <div class="col-sm-3 adv-tile">
        <div class="img-header accessories"><img src="images/products/decking/decking_accessories_3.jpg" alt="Fastener" />
          <h4>Fastener</h4>
        </div>
      </div>
      <div class="col-sm-3 adv-tile">
        <div class="img-header accessories"><img src="images/products/decking/decking_accessories_4.jpg" alt="Covered Edge" />
          <h4>Covered Edge</h4>
        </div>
      </div>
    </div>
  </div>
</div>


<?php include 'inc/highlights.php';?>
<?php include 'inc/services.php';?>
<?php include 'inc/footer.php';?>